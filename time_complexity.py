from timeit import default_timer as timer
from typing import List, TextIO

import copy
import random

import matplotlib.pyplot as plt # type: ignore
import numpy as np # type: ignore


def dummy (my_list: List[int]) -> None:

  file: TextIO = open("dummy.csv", "a")
  start: float = timer()
  item: int

  for item in my_list:

    if item >= 0:
      file.write("{};{}\n".format(len(my_list), timer() - start))
      break


def do_nothing (my_list: List[int]) -> None:

  file: TextIO = open("do_nothing.csv", "a")
  start: float = timer()
  item: int

  for item in my_list:
    continue


  file.write("{};{}\n".format(len(my_list), timer() - start))


def bubble_sort (list_to_sort: List[int]) -> None:

  file: TextIO = open("bubble_sort.csv", "a")
  start: float = timer()

  my_list: List[int] = copy.deepcopy(list_to_sort)
  please_sort: bool = True

  while please_sort:

    please_sort = False
    index: int

    for index in range(1, len(my_list)):

      if my_list[index] < my_list[index - 1]:
        please_sort = True
        tmp: int = my_list[index]
        my_list[index] = my_list[index - 1]
        my_list[index - 1] = tmp


  file.write("{};{}\n".format(len(my_list), timer() - start))


def insertion_sort (list_to_sort: List[int]) -> None:

  file: TextIO = open("insertion_sort.csv", "a")
  start: float = timer()

  my_list: List[int] = copy.deepcopy(list_to_sort)
  
  i: int
  j: int

  for i in range(1, len(my_list)):
    for j in range(i, 0, -1):

      if my_list[j - 1] <= my_list[j]:
        break
    
      my_list[j], my_list[j - 1] = my_list[j - 1], my_list[j]
    

  file.write("{};{}\n".format(len(my_list), timer() - start))


def selection_sort (list_to_sort: List[int]) -> None:
  
  file: TextIO = open("selection_sort.csv", "a")
  start: float = timer()

  my_list: List[int] = copy.deepcopy(list_to_sort)
  passes_left: int
  index: int


  for passes_left in range(0, len(my_list) - 1, +1):
    min_number: int = passes_left        
    
    for index in range(passes_left + 1, len(my_list), +1):
      if my_list[index] < my_list[min_number]:
        min_number = index
    
    my_list[min_number], my_list[passes_left] = my_list[passes_left], my_list[min_number]        


  file.write("{};{}\n".format(len(my_list), timer() - start))


def graph (file: TextIO) -> List[list]:

  x_points: List[int] = []
  y_points: List[float] = []


  raw: str

  for raw in file:
    elements: List[str] = raw.split(';')
    x_points.append(int(elements[0]))
    y_points.append(float(elements[1]))


  return [x_points, y_points]


# MAIN

open("dummy.csv", "w")
open("do_nothing.csv", "w")
open("bubble_sort.csv", "w")
open("insertion_sort.csv", "w")
open("selection_sort.csv", "w")


i: int
n: int

for n in range(1, 1001):
  
  NUMBERS: List[int] = []

  for i in range(n):
    NUMBERS.append(random.randrange(1000))


  dummy(NUMBERS)
  do_nothing(NUMBERS)
  bubble_sort(NUMBERS)
  insertion_sort(NUMBERS)
  selection_sort(NUMBERS)


FILES: List[TextIO] = [
  
  {
    "file": open("dummy.csv", "r"),
    "color": "#069",
    "label": "dummy"
  },
  
  {
    "file": open("do_nothing.csv", "r"),
    "color": "#069",
    "label": "do_nothing"
  },
  
  {
    "file": open("bubble_sort.csv", "r"),
    "color": "#f00",
    "label": "bubble_sort"
  },
  
  {
    "file": open("insertion_sort.csv", "r"),
    "color": "#f90",
    "label": "insertion_sort"
  },
  
  {
    "file": open("selection_sort.csv", "r"),
    "color": "#393",
    "label": "selection_sort"
  }
  
]


file: TextIO

for file in FILES:
  xy: List[list] = graph(file["file"])
  plt.plot(xy[0], xy[1], label = file["label"], color = file["color"])


plt.legend()
plt.show()