import copy


def bubble_sort(numbers_to_sort):

  please_loop = True
  sorted_list = copy.deepcopy(numbers_to_sort)

  # sorted_list = [-2, 4, 8, 12, 29, 36, -17, 45, 2, 12, 12, 3, 3, -52, 78]
  

  while please_loop:

    please_loop = False

    for i in range(1, len(sorted_list)):

      if sorted_list[i] < sorted_list[i - 1]:

        please_loop = True

        # sorted_list[i], sorted_list[i-1] = sorted_list[i-1], sorted_list[i]

        temp = sorted_list[i] # => 12
        sorted_list[i] = sorted_list[i-1] # sorted_list[i] => -2
        sorted_list[i-1] = temp # sorted_list[i-1] => 12


def insertion_sort(numbers_to_sort):

  sorted_list = copy.deepcopy(numbers_to_sort)


def selection_sort(numbers_to_sort):

  sorted_list = copy.deepcopy(numbers_to_sort)


# list is not sorted
numbers = [12, -2, 4, 8, 29, 45, 78, 36, -17, 2, 12, 12, 3, 3, -52]

# print the smallest number
# print the biggest number
# print all even numbers
# how often does the first number appear in the list?
# print the average
# print the sum of all the negative numbers

# sort = display the list this way:
# [-52, -17, -2, 2, 3, 3, 4, 8, 12, 12, 12, 29, 36, 45, 78]

# sort the list using bubble sort
bubble_sort(numbers)

# sort the list using insertion sort
insertion_sort(numbers)

# sort the list using selection sort
selection_sort(numbers)
